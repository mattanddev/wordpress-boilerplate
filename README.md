# Wp (with timber) via composer

> note: this setups works with [aweseome.dev](https://gitlab.com/mattanddev/awesome.dev) environment. Just clone the repo in the ```./www/``` folder of your awesome.dev machine.


## Setup 

### Folder and htaccess

Rename the ```wordpress-boilerplate/``` to the name of your project and reflect the changes into the ```.htaccess``` rewrite mode block.
This is needed by the vagrant machine for proper url mapping.

### Configuration

Within your ```wp-config.php``` set the name of your ev db and default theme.


### Install dependencies

Intall all base dependencies via composer using ```$ composer install```.
This will setup wp and timber, for more info on composer refer to [this](https://getcomposer.org/).



### Setup the wp instance

Navigate to ```awesome.dev/your-project-name``` and the wp installation screen will pop up.
After setting base configs remember to:
- enable Timber
