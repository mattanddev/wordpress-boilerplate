<?php

// ===================================================
// Custom wp-config to speed up set-up
// ===================================================
// This config will look for deev environment setup.
// If true, it will build the database and set dev environment.
// To move to production just add production-config.php to the root
// ===================================================


// Set te DB
$db = 'test_projec_db';

// Set the default theme
define('WP_DEFAULT_THEME', 'test-theme');


// Override default wp behaviour
$folder  = basename(__DIR__) ;
define('WP_HOME','http://awesome.dev/' . $folder );


// If not existing set up a 
// ===================================================


// Connect to host
$connection = mysqli_connect('localhost', 'root', 'ao2013dev');

// create DB if it doesn't exist
mysqli_query( $connection, 'CREATE DATABASE IF NOT EXISTS ' . $db);
mysqli_select_db($connection, $db);
define( 'DB_NAME', $db );

ini_set( 'display_errors', 'On' );




// ===================================================
// Load database info and local development parameters
// ===================================================
if ( file_exists( dirname( __FILE__ ) . 'production-config.php' ) ) {
    define( 'WP_LOCAL_DEV', false );
    include( dirname( __FILE__ ) . 'production-config.php' );
} else {
    define( 'WP_LOCAL_DEV', true );
    include( dirname( __FILE__ ) . '/../_wp_environment.php' );
}

// ========================
// Custom Content Directory
// ========================
define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content' );
define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/wp-content' );

// ================================================
// You almost certainly do not want to change these
// ================================================
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

// ================================
// Language
// Leave blank for American English
// ================================
define( 'WPLANG', '' );

// ======================
// Hide errors by default
// ======================
define( 'WP_DEBUG_DISPLAY', false );
define( 'WP_DEBUG', false );

// =========================
// Disable automatic updates
// =========================
define( 'AUTOMATIC_UPDATER_DISABLED', false );

// =======================
// Load WordPress Settings
// =======================
$table_prefix  = 'wp_';

if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/wp/' );
}
require_once( ABSPATH . 'wp-settings.php' );